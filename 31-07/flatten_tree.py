def un_flatten(paths):
    d = {}
    max_lens = 0
    new_paths = []
    for path in paths:
        x = path.split("/")
        new_paths.append(x)
    for path in new_paths:
        if len(path) > max_lens:
            max_lens = len(path)

    l = []
    for i in xrange(max_lens):
        l.append([])
        for path in new_paths:
            if i < len(path):
                l[i].append(path[i])
        l[i] = set(l[i])

    print l
    print new_paths
    #return d


def test_un_flatten():
    tree = un_flatten(['A/B/T', 'A/U', 'A/U/Z'])
    assert tree == {
        'A': {
            'B': {'T': {}},
            'U': {'Z': {}
            }
        }
    }
    assert tree['A']['B']['T'] == {}

un_flatten(['A/B/T', 'A/U', 'A/U/Z'])