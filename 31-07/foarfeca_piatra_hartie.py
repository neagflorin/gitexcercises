import random

choices = ["hartie","piatra","foarfeca"]

player1_wins = 0
player2_wins = 0
ties = 0

wins = [("hartie,piatra"), ("piatra","foarfeca"), ("foarfeca","hartie")]

for i in xrange(10):
    player1 = random.choice(choices)
    player2 = random.choice(choices)
    if player1 == player2:
        ties += 1
    elif (player1,player2) in wins:
        player1_wins += 1
    else:
        player2_wins += 1

print "p1 wins = ", player1_wins
print "p2 wins = ", player2_wins
print "ties= ", ties