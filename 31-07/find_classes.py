import os
import re


def prepare(dir_path,filter):
    if not os.path.isdir(dir_path):
        raise ValueError()
    else:
        tree(dir_path,filter)

def tree(dir_path,filter):
    for item in os.listdir(dir_path):
        item_path = os.path.join(dir_path, item)
        if os.path.isdir(item_path):
            tree(item_path,filter)
        elif item[-3:] == ".py":
            with open(item_path) as f:
                for line in f:
                    if re.search(filter,line):
                        print item + " : " + line


if __name__ == "__main__":
    try:
        prepare("/home/florin/","class")
    except ValueError:
        print "Dati un director"