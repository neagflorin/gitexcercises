def create_pgm(filename,header,pixels):
    with open(filename, "wb") as pgm:
        pgm.write(header)
        for p in pixels:
            pgm.write(chr(p))


def img1_pixels(dimensiune):
    interval = dimensiune / 100 if dimensiune / 100 > 1 else 2
    pixels = []
    p_start = -1
    for i in xrange(dimensiune):
        if i % interval == 0:
            p_start += 1
        p = p_start
        for j in xrange(dimensiune):
            pixels.append(p)
            if j % interval == 0:
                p += 1
    return pixels

def img2_pixels(dimensiune):
    alb = set()
    pixels = []
    x = dimensiune/10
    for i in xrange(0,2*dimensiune,x):
        alb.update(xrange(i,i+x/2))
    for i in xrange(dimensiune):
        for j in xrange(dimensiune):
            if i+j in alb:
                pixels.append(255)
            else:
                pixels.append(0)
    return pixels


dimensiune = 540
header = 'P5' + '\n' + str(dimensiune) + '  ' + str(dimensiune) + '  ' + "255" + '\n'
pixels = img1_pixels(dimensiune)
create_pgm("img1.pgm",header,pixels)
pixels = img2_pixels(dimensiune)
create_pgm("img2.pgm",header,pixels)
