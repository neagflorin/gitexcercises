# exemplu
# scrie un program care afiseaza imaginea
# o . . . .
# o o . . .
# o o o . .
# o o o o .
# o o o o o

def pattern1():
    for i in xrange(5):
        for j in xrange(5):
            if i >= j:
                print "o",
            else:
                print ".",
        print



# exercitiu
# o . . . o
# o o . o o
# o o o o o
# o o . o o
# o . . . o
def pattern2():
    N = 5
    for i in xrange(1,N+1):
        for j in xrange(1,N+1):
            if (i < j and i + j <= N) or (i > j and i + j >= N+2):
                print ".",
            else:
                print "o",
        print
