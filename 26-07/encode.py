def encode(list):
    out = []
    i = 0
    nr_aparitii = 1
    while i < len(list)-1:
        if list[i] == list[i+1]:
            nr_aparitii += 1
        else:
            out.append((nr_aparitii,list[i]))
            nr_aparitii = 1
        i += 1
    if list[i] == list[i-1]:
        out.append((nr_aparitii,list[i]))
    else:
        out.append((1,list[i]))
    return out


def decode(list):
    out = []
    for e in list:
        nr_apariti = e[0]
        while nr_apariti:
            out.append(e[1])
            nr_apariti -= 1
    return out


def test_encode():
    assert encode([0, 0, 0, 1, 1, 7, 0, 0, 0, 3]) == [(3, 0), (2, 1), (1, 7), (3, 0), (1, 3)]
    assert encode([0, 0, 0, 1, 1, 7, 0, 0, 0, 0]) == [(3, 0), (2, 1), (1, 7), (4, 0)]

def test_decode():
    assert decode([(3, 0), (2, 1), (1, 7), (4, 0)]) == [0, 0, 0, 1, 1, 7, 0, 0, 0, 0]
    assert decode([(3, 0), (2, 1), (1, 7), (3, 0), (1, 3)]) == [0, 0, 0, 1, 1, 7, 0, 0, 0, 3]

def encode2(list):
    out = []
    i = 0
    nr_aparitii = 1
    while i < len(list) - 1:
        if list[i] == list[i + 1]:
            nr_aparitii += 1
        else:
            out.append(nr_aparitii)
            out.append(list[i])
            nr_aparitii = 1
        i += 1
    if list[i] == list[i - 1]:
        out.append(nr_aparitii)
        out.append(list[i])
    else:
        out.append(1)
        out.append(list[i])
    return out

def decode2(list):
    out = []
    for i in xrange(0,len(list)-1,2):
        nr_apariti = list[i]
        while nr_apariti:
            out.append(list[i+1])
            nr_apariti -= 1
    return out

def test_encode2():
    assert encode2([0, 0, 0, 1, 1, 7, 0, 0, 0, 3]) == [3, 0, 2, 1, 1, 7, 3, 0, 1, 3]
    assert encode2([0, 0, 0, 1, 1, 7, 0, 0, 0, 0]) == [3, 0, 2, 1, 1, 7, 4, 0]

def test_decode2():
    assert decode2([3, 0, 2, 1, 1, 7, 4, 0]) == [0, 0, 0, 1, 1, 7, 0, 0, 0, 0]
    assert decode2([3, 0, 2, 1, 1, 7, 3, 0, 1, 3]) == [0, 0, 0, 1, 1, 7, 0, 0, 0, 3]


test_decode()