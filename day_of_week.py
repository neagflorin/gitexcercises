"""
Day of the week. 
Write a program that takes a date as input and prints the day of 
the week that date falls on. 
Your program should take three command-line arguments: m (month), d (day), and y (year). 
For m use 1 for January, 2 for February, and so forth. 
For output print 0 for Sunday, 1 for Monday, 2 for Tuesday, and so forth. 
Use the following formulas, for the Gregorian calendar (where / denotes integer division):



For example, on which day of the week was August 2, 1953?
"""

import sys

def day_of_week(m,d,y):
    y0 = y - (14 - m) // 12
    x = y0 + y0 // 4 - y0 // 100 + y0 // 400
    m0 = m + 12 * ((14 - m) // 12) - 2
    d0 = (d + x + 31 * m0 // 12) % 7
    print(d0)


if len(sys.argv) == 4:
    m = int(sys.argv[1])
    d = int(sys.argv[2])
    y = int(sys.argv[3])
    day_of_week(m,d,y)