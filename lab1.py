# Exercises for notions learned in unit 1
# ---------------------------------------

# takes a message, capitalizes the first letter then underlines the message with =
# print make_title('ana')
# Ana
# ===
# use the string method upper
from __builtin__ import reversed


def make_title(message):
    return message.capitalize() + "\n" + "="*len(message)

# given a list of tuples of length 2 it returns a list of the tuples inverted
# if both values in the tuple are the same then we omit them from the output
# [(2, 4), (3, 3), (1, 2)] will become [(4, 2), (2, 1)]
def invert_tuples(lst):
    output = []
    for a,b in lst:
        if a != b:
            output.append((b,a))
    return output

# curses is a dictionary of cursewords to censored curses
# Given a list of words replace the curses according to the dict
# censor(['you', 'fucker'], {'fucker': 'f**er'}) should return
# ['you', 'f**er']
def censor(lst, curses):
    for i,x in enumerate(lst):
        if x in curses:
            lst[i] = curses[x]
    return lst

# Given a string s, return a string made of the first 2
# and the last 2 chars of the original string,
# so 'spring' yields 'spng'. However, if the string length
# is less than 2, return instead the empty string.
def both_ends(s):
    if len(s) < 2:
        return ""
    return s[:2]+s[-2:]

# return the factorial of n
# factorial(5) = 1*2*3*4*5
def factorial(n):
    a = 1
    for i in xrange(1,n+1):
        a *= i
    return a


def invert_dict(d):
    """
    >>> invert_dict({'ana': 4, 'are': 5})
    {4: 'ana', 5: 'are'}
    """
    d2 = {}
    for k,v in d.items():
        d2[v] = k
    return d2

def has_no_repeating_chars(string):
    """
    in the word "eleven" e appears 3 times.
    return True if string contains no repeated letters
    >>> has_no_repeating_chars('eleven')
    False
    >>> has_no_repeating_chars('')
    True
    >>> has_no_repeating_chars('python')
    True
    """
    for i,c in enumerate(string):
        if c in string[i+1:]:
            return False
    return True

def flatten_list(lst):
    """
    lst is a list. It may contain other lists.
    Return a flattened version of lst
    >>> flatten_list([1, 2, [3, 4], [5, [6, 7], 8]])
    [1, 2, 3, 4, 5, 6, 7, 8]
    >>> flatten_list([ [44, 11], [99, 33] ])
    [44, 11, 99, 33]
    """
    if len(lst) == 0:
        return []
    if isinstance(lst[0],list):
        return flatten_list(lst[0]) + flatten_list(lst[1:])
    else:
        return [lst[0]] + flatten_list(lst[1:])

def splits(word):
    """
    returns a list of all posible splits of word
    include the empty splits
    returns a list of tuples.
    >>> splits('ana')
    [('', 'ana'), ('a', 'na'), ('an', 'a'), ('ana', '')]
    """
    sp = [(word[:i],word[i:]) for i in xrange(len(word)+1)]
    return sp

def inserts(word):
    """
    returns all misspellings of word caused by inserting a letter
    use the splits function
    'bana' in inserts('ana')
    True
    'naa' not in inserts('ana')
    True
    """
    letters = 'abcdefghijklmnopqrstuvwxyz'
    sp = splits(word)
    ins = [st + l + dr for l in letters for st,dr in sp]
    return ins

def deletes(word):
    sp = splits(word)
    deletes = [sp[i][0]+sp[i+1][1] for i in xrange(len(sp)-1)]
    return deletes

def transposes(word):

    sp = splits(word)
    tr = [st + dr[1] + dr[0] + dr[2:] for st,dr in sp if len(dr)>1]
    return tr

def replaces(word):
    # [('', 'ana'), ('a', 'na'), ('an', 'a'), ('ana', '')]
    letters = 'abcdefghijklmnopqrstuvwxyz'
    sp = splits(word)
    rep = [st + l + dr[1:] for l in letters for st,dr in sp[:-1]]
    return rep

#----- testing code below ---

def test_make_title():
    assert make_title('ana') == 'Ana\n==='
    assert make_title("python") == "Python\n======"

def test_invert_tuples():
    assert invert_tuples([(2, 4), (3, 3), (1, 2)]) == [(4, 2), (2, 1)]

def test_censor():
    assert censor(['you', 'fucker', 'bitch'], {'fucker': 'f**er','bitch': 'b**ch'}) == ['you', 'f**er', 'b**ch']

def test_both_ends():
    assert both_ends('spring') == 'spng'
    assert both_ends('Hello') == 'Helo'
    assert both_ends('a') == ''
    assert both_ends('xyz') == 'xyyz'

def test_factorial():
    assert factorial(5) == 1*2*3*4*5
    assert factorial(1) == 1
    assert factorial(0) == 1

