import os
import sys


def prepare(dir_path):
    identation = 1
    if not os.path.isdir(dir_path):
        raise ValueError()
    elif dir_path[-1] == "/":
        dir_path = dir_path[:-1]
    tree(dir_path,identation)

def tree(dir_path,identation):
    dir = dir_path.split("/")
    dir = dir[-1]
    print "|  "*identation, "-", dir
    for item in os.listdir(dir_path):
        item_path = os.path.join(dir_path, item)
        if os.path.isdir(item_path):
            tree(item_path,identation+1)
        else:
            print "|  "*(identation+1), "-", item


if __name__ == "__main__":
    try:
        prepare(sys.argv[1])
    except ValueError:
        print "Dati un director"