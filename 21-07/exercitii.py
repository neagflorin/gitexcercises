import turtle
import random

t = turtle.Turtle()

def desen(n,distanta):
    while n:
        t.forward(distanta)
        t.right(90)
        n -= 1


def spirala(n):
    distanta = n * 30
    desen(3,distanta)
    distanta -= 30
    for i in xrange(n):
        desen(2,distanta)
        distanta -= 30

    turtle.done()


def ekg():
    randoms = []
    for i in xrange(100):
        rnd = random.random()
        randoms.append(rnd)
    for x,y in enumerate(randoms):
        t.goto(x*10,y*30)
        t.dot()

