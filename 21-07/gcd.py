def gcd(a,b):
    while b != 0:
        t = b
        b = a % b
        a = t
    return a

def test_gcd():
    assert gcd(3,12) == 3
    assert gcd(1,124) == 1
    assert gcd(0,91) == 91
    assert gcd(8,0) == 8

test_gcd()