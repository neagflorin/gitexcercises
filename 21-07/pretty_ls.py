#!/usr/bin/env python
import os
import colored_term as color
import sys


def get_size(file_bytes):
    if file_bytes < 1024:
        return str(file_bytes)+".0b"
    elif file_bytes >= 1024 and file_bytes <= 1048576:
        kb = float(file_bytes) /1024
        return "{0:.1f}".format(kb)+"kb"
    else:
        mb = float(file_bytes) /1048576
        return "{0:.1f}".format(mb)+"Mb"


def ls_content(dir_path):

    list_dir = {}
    list_file = {}
    for item in os.listdir(dir_path):
        item_path = os.path.join(dir_path,item)
        if os.path.isdir(item_path):
            nr_items = 0
            for item2 in os.listdir(item_path):
                subitem_path = os.path.join(item_path, item2)
                if os.path.isdir(subitem_path):
                     nr_items += 1
            list_dir[item+"/"] = str(nr_items) + " items"
        else:
            list_file[item] = get_size(os.path.getsize(item_path))

    return list_dir,list_file


def pretty_ls(path):
    dirs, files = ls_content(path)
    for k,v in dirs.iteritems():
        if k[0] == ".":
            director = color.colored_text(k,color.LBLACK)
            dir_items = color.colored_text(v, color.LBLACK)
        else:
            director = color.colored_text(k, color.LBLUE)
            dir_items = color.colored_text(v, color.LBLUE)
        print "{:<40} {:>15}".format(director,dir_items)
    for k,v in files.iteritems():
        if k[-3:] == ".py":
            file = color.colored_text(k,color.LGREEN)
            file_size = color.colored_text(v,color.LGREEN)
        else:
            file = color.colored_text(k, color.DPURPLE)
            file_size = color.colored_text(v, color.DPURPLE)
        print "{:<40} {:>15}".format(file, file_size)

if __name__ == "__main__":
    if len(sys.argv) > 1:
        pretty_ls(sys.argv[1])
    else:
        pretty_ls(".")