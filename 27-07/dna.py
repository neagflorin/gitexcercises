import urllib2
from bs4 import BeautifulSoup

def get_links(url, word, nr_news):
    soup = BeautifulSoup(urllib2.urlopen(url),'html.parser')
    links_with_word = []
    for link in soup.find_all('a'):
        if word in str(link.get('href')):
            links_with_word.append(link.get('href'))
    links_with_word = list(set(links_with_word))
    links_with_word = links_with_word[:nr_news]
    return links_with_word

def get_news(url,word,nr_news):
    links = get_links(url,word,nr_news)
    for link in links:
        soup = BeautifulSoup(urllib2.urlopen(link), 'html.parser')
        div = soup.find("div",{"id" : "articleContent"})
        if div:
            print div.get_text()


if __name__ == "__main__":

    url = "http://www.hotnews.ro/"
    word = "dna"
    nr_news = 2
    get_news(url,word,nr_news)