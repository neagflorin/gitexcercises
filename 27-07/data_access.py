import sqlite3
import csv

def get_artists():
    conn = sqlite3.connect("chinook.db")
    cursor = conn.cursor()
    cursor.execute("select * from artists")
    artists = cursor.fetchall()
    conn.close()
    return artists


def get_artists_with_tracks_10_plus_minutes():
    conn = sqlite3.connect("chinook.db")
    cursor = conn.cursor()
    cursor.execute("""select a.Name,al.Title,t.Name,sum(t.Milliseconds) from artists a inner join albums al on a.ArtistId = al.ArtistId
    inner join tracks t on al.AlbumId = t.AlbumId group by a.ArtistId having sum(t.Milliseconds)>600000""")
    artists_tracks = cursor.fetchall()
    conn.close()
    return  artists_tracks



def print_artists(page=1,page_size=10):
    artists = get_artists()
    artists = artists[(page-1)*page_size:page*page_size]
    artists_names = ["".join(a[1]).encode('utf-8') for a in artists]
    len_names = [len(name) for name in artists_names]
    max_len_name = max(len_names)
    print "{0} {1} {0}".format("+","-"*max_len_name)
    for artist in artists_names:
        print "| {0:^{1}} |".format(artist,max_len_name)
        print "{0} {1} {0}".format("+", "-" * max_len_name)


def print_artists_tracks():
    artists = get_artists_with_tracks_10_plus_minutes()
    artists_names = ["".join(x[0]).encode('utf-8') for x in artists]
    artists_albums =  ["".join(x[1]).encode('utf-8') for x in artists]
    artists_tracks = ["".join(x[2]).encode('utf-8') for x in artists]
    artists_minutes = [str(x[3]/60000) for x in artists]

    len_names = [len(name) for name in artists_names]
    max_len_name = max(len_names)
    len_albums = [len(name) for name in artists_albums]
    max_len_album = max(len_albums)
    len_tracks = [len(name) for name in artists_tracks]
    max_len_track = max(len_tracks)
    max_len_minute = len("minutes")

    print "{0} {1} {0} {2} {0} {3} {0} {4} {0}".format\
        ("+", "-"*max_len_name, "-"*max_len_album, "-"*max_len_track, "-"*max_len_minute)

    print "| {0:^{1}} | {2:^{3}} | {4:^{5}} | {6:^{7}} |".format("artist", max_len_name,
                                                            "album",max_len_album,
                                                            "track",max_len_track,
                                                            "minutes",max_len_minute)
    print "{0} {1} {0} {2} {0} {3} {0} {4} {0}".format\
        ("+", "-"*max_len_name, "-"*max_len_album, "-"*max_len_track, "-"*max_len_minute)

    for i in xrange(len(artists)):
        print "| {0:^{1}} | {2:^{3}} | {4:^{5}} | {6:^{7}} |".format\
            (artists_names[i], max_len_name,
            artists_albums[i], max_len_album,
            artists_tracks[i], max_len_track,
            artists_minutes[i], max_len_minute)
        print "{0} {1} {0} {2} {0} {3} {0} {4} {0}".format\
            ("+", "-"*max_len_name, "-"*max_len_album, "-"*max_len_track, "-" * max_len_minute)


def artists_to_csv_file(filename):
    artists = get_artists()
    artists = [(a[0] , "".join(a[1]).encode('utf-8')) for a in artists]
    with open(filename,"w") as csv_file:
        writer = csv.writer(csv_file)
        header = ["artistId", "artistName"]
        writer.writerow(header)
        for a in artists:
            writer.writerow(a)

def artists_tracks_to_csv_file(filename):
    artists = get_artists_with_tracks_10_plus_minutes()
    artists_names = ["".join(x[0]).encode('utf-8') for x in artists]
    artists_albums = ["".join(x[1]).encode('utf-8') for x in artists]
    artists_tracks = ["".join(x[2]).encode('utf-8') for x in artists]
    artists_minutes = [str(x[3] / 60000) for x in artists]
    artists = zip(artists_names,artists_albums,artists_tracks,artists_minutes)
    with open(filename, "w") as csv_file:
        writer = csv.writer(csv_file)
        header = ["artistName", "Album", "Track", "Minutes"]
        writer.writerow(header)
        for a in artists:
            writer.writerow(a)

artists_tracks_to_csv_file("artists_tracks.csv")
artists_to_csv_file("artists.csv")