#!/usr/bin/python

class Todo(object):
    def __init__(self, message, id_=None, done=False, created=None):
        self.message = message
        self.id = id_
        self.done = done
        self.created = created

    def setDone(self):
        self.done=True

    def __str__(self):
        return  'id:{} message:{} done:{} created: {}'.format(self.id, self.message, self.done, self.created)



