import prettydate

def todos_html_format(filename,todos):
    header = "<!DOCTYPE html>\n<html>\n<head>\n<title>Todos</title>\n</head>\n<body>\n"
    footer = "</body>\n</html>"
    with open(filename, "w") as f:
        f.write(header)
        for todo in todos:
            f.write("<div>{0} {1} {2} {3}</div>\n".format(todo.id,
                                                        todo.message,
                                                        todo.done,
                                                        prettydate.pretty_date(todo.created)))
        f.write(footer)
