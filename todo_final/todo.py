import color_terrm as ct
from DBJson import DBJson
from DBSqlite import DBSqlite
import prettydate
import config
import toXml
import export_html
import sys
import argparse

def printTodo(todo):
    print "id " + ct.colored_text(str(todo.id),ct.GREEN) + " todo: " +\
          "{0:<15} {1:>10}".format(todo.message,
                               ct.colored_text(prettydate.pretty_date(todo.created), ct.LBLUE))

def getDB():
    if config.STORAGE_TYPE=='json':
        DB=DBJson
    else:
        DB=DBSqlite
    db=DB(config.STORAGE_PATH)
    return db

def command_add(msg):
    db=getDB()
    db.add(msg)
    print "Todo added"

def command_done(id):
    db=getDB()
    db.setDone(id)
    print"Todo setted to done"

def command_list(where=None,file=None):
    db = getDB()
    if where=='xml':
        toXml.toXml(db.todo_list,file)
    elif where=='html':
        export_html.todos_html_format(file,db.todo_list)
    else:
        for todo in db.todo_list:
            if not todo.done:
                printTodo(todo)

if __name__ == "__main__":
    '''
    am incercat cateva variante dar nu :(((
    
    parser = argparse.ArgumentParser()
    group = parser.add_argument_group()
    group.add_argument("list",help="Lists the todos")
    group.add_argument("add", help="Add a todo",nargs="+")
    group.add_argument("done",help="Set todo to done",nargs="+",type=int)
    
    group2 = parser.add_mutually_exclusive_group()
    group2.add_argument("--xml",action="store_true")
    group2.add_argument("--html",action="store_true")
    
    args = parser.parse_args()

    if args.list:
        command_list("x")
    elif args.add:
        command_add(args.add[1])
    else:
        print args.cmd
    '''

    if len(sys.argv) > 1:
        cmd = sys.argv[1]
        if cmd == 'list':
            if len(sys.argv) == 2:
                command_list()
            else:
                export_format = sys.argv[2]
                export_file = "todos."+export_format
                command_list(export_format, export_file)
        elif cmd == 'done':
            command_done(int(sys.argv[2]))
        elif cmd == 'add':
            message = " ".join(sys.argv[2:])
            command_add(message)
        else:
            print "Command " + cmd + " not found"

