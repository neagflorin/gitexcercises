from todoObj import Todo
import sqlite3
from datetime import datetime

class DBSqlite(object):
    def __init__(self,filename):
        self.filename=filename
        self.todo_list = []
        self.load(self.filename)

    def load(self,filename):
        conn = sqlite3.connect(filename)
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM todo")
        todos=cursor.fetchall()
        for t in todos:
            self.todo_list.append(self._todo_from_db(t))
        conn.close()

    def _todo_from_db(self,todo):
        format = "%Y-%m-%dT%H:%M:%S.%f"
        if todo[2] == 'True':
            todoObj = Todo(todo[1], todo[0], True, datetime.strptime(todo[3], format))
        else:
            todoObj = Todo(todo[1], todo[0], False, datetime.strptime(todo[3], format))
        return todoObj

    def _ensure_id(self):
        return len(self.todo_list) + 1


    def save(self,filename):
        conn = sqlite3.connect(filename)
        cursor = conn.cursor()
        todo=self.todo_list[-1]
        cursor.execute("INSERT INTO todo values(?,?,?,?)",(todo.id , todo.message, str(todo.done), todo.created.isoformat()))
        conn.commit()
        conn.close()

    def add(self, t):
        message = t
        id = self._ensure_id()
        created = datetime.now()
        t = Todo(message, id, False, created)
        self.todo_list.append(t)
        self.save(self.filename)

    def get(self, id):
        for todo in self.todo_list:
            if todo.id==id:
                return todo

    def setDone(self,id):
        conn= sqlite3.connect(self.filename)
        cursor = conn.cursor()
        cursor.execute("UPDATE todo SET done='True' WHERE id={}".format(id))
        self.todo_list=[]
        self.load(self.filename)
        conn.commit()
        conn.close()


