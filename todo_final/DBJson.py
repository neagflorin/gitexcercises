import json
from todoObj import Todo
from datetime import datetime


class DBJson(object):
    def __init__(self,filename):
        self.filename = filename
        self.todo_list = []
        self.load(filename)

    def load(self,filename):
        with open(filename) as f:
            for line in f:
                todo = self._todo_from_json(line)
                self.todo_list.append(todo)


    def _ensure_id(self):
        return len(self.todo_list) + 1

    def _todo_to_json(self, obj):
        jsonTodo = {}
        jsonTodo["message"] = obj.message
        jsonTodo["id"] = obj.id
        jsonTodo["done"] = str(obj.done)
        jsonTodo["created"] = obj.created.isoformat()
        return json.dumps(jsonTodo)

    def _todo_from_json(self, rt):
        todo = json.loads(rt)
        format = "%Y-%m-%dT%H:%M:%S.%f"
        if todo["done"] == 'True':
            todoObj = Todo(todo['message'], todo['id'], True, datetime.strptime(todo['created'], format))
        else:
            todoObj = Todo(todo['message'], todo['id'], False, datetime.strptime(todo['created'], format))
        return todoObj

    def save(self,filename):
        with open(filename, 'w') as f:
            for todo in self.todo_list:
                f.write(self._todo_to_json(todo) + "\n")

    def add(self, t):
        message = t
        id = self._ensure_id()
        created = datetime.now()
        t = Todo(message, id, False, created)
        self.todo_list.append(t)
        self.save(self.filename)

    def get(self, id):
        for todo in self.todo_list:
            if todo.id == id:
                return todo

    def setDone(self, id):
        for todo in self.todo_list:
            if todo.id == id:
                todo.setDone()
        self.save(self.filename)


