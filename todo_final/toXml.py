import prettydate

def toXml(list,file):
    with open(file,'w') as f:
        f.write('<?xml version="1.0"?>\n')
        f.write('<data>\n')
        for todo in list:
            f.write('   <todo name="')
            f.write(todo.message)
            f.write('">\n')
            f.write('       <id>')
            f.write(str(todo.id))
            f.write("</id>\n")
            f.write('       <done>')
            f.write(str(todo.done))
            f.write("</done>\n")
            f.write('       <created>')
            f.write(prettydate.pretty_date(todo.created))
            f.write("</created>\n")
            f.write('   </todo>\n')
        f.write('</data>\n')

