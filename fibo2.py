def fibo(upto):
    ret = []
    a,b = 1,1
    while a < upto:
        a, b = b, a+b
        ret.append(a)
    return ret

def test_fibo():
    assert fibo(10) == [1,1,2,3,5,8]

