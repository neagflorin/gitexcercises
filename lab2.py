import re
from collections import Counter


def wordcounts(pth):
    # todo: count words in file
    # todo: print words in descending order of frequency
    with open(pth) as file:
        words = Counter(re.findall(r'\w+',file.read().lower()))
    return sorted(words.items(),key = lambda x:x[1], reverse=True)


x = wordcounts('corpus.txt')
for word,frecv in x:
    print word,frecv